@extends('layouts.master')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Students</div>

                <div class="panel-body">
                    <table id="salhitable" class="ui celled table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>Name</td>
                            <td>Section</td>
                            <td>Email</td>
                            <td>Pending Invoice</td>
                        </tr>
                        </thead>
                       <tbody>
                       @foreach($studentData as $student)
                           <tr>
                               <td>{{$student->firstName}}</td>
                               <td>{{$student->sectionId}}</td>
                               <td>{{$student->email}}</td>
                               <td> @if(Count($student->invoiceId) > 0)
                                    @foreach($student->invoiceId as $invoice)
                                           <span><?php $date = date_create($invoice->due_date); echo (date_format($date, 'F Y')); ?></span> :
                                           <a href="">{{ $invoice->invoice_number }}</a> <br>
                                    @endforeach
                                   @endif
                               </td>
                           </tr>
                       </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
