@extends('voyager::master')
@section('content')
    <div class="page-content container-fluid">
      <div class="container is-fluid" style="width:100%;">
        <div class="flex-center full-height">
          <form role="form" method="POST"
                    id="divToPrint"
                    class="form-edit-add"
                    action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                    method="POST" enctype="multipart/form-data">
                <!-- PUT Method if we are editing -->
                @if(isset($dataTypeContent->id))
                    {{ method_field("PUT") }}
                @endif

                <!-- CSRF TOKEN -->
                {{ csrf_field() }}

                <div class="panel-body">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <!-- If we are editing -->
                    @if(isset($dataTypeContent->id))
                        <?php $dataTypeRows = $dataType->editRows; ?>
                    @else
                        <?php $dataTypeRows = $dataType->addRows; ?>
                    @endif


          <img src="/img/gems.png">
          <ul>
              <li>Student name: {{$dataTypeContent->studentId->name}}</li>
              <li>Class:{{$dataTypeContent->batchId->name}} </li>
          </ul>
          <table class="ui celled table" cellspacing="0" width="100%" id="salhitable">
                <tbody>
                  <div class="table is-narrow is-bordered full-height container">
                        <ul style ="justify-content:flex-end;">
                          @if(isset($dataTypeContent->id))
                                <li> Invoice ID: {{$dataTypeContent->invoice_number}} </li>
                                <li> Status: {{$dataTypeContent->status}} </li>
                                <li> Billing Date: {{$dataTypeContent->due_date}}</li>
                            @endif
                        </ul>
                       <thead>
                          <th>Fee Description</th>
                          <th>Qty</th>
                          <th>Amount</th>
                          <th>Total</th>
                        </thead>
                          <tr class="is-selected">
                            <td> Monthly </td>
                            <td> 1 </td>
                            <td> 600 </td>
                            <td> 600 </td>
                          </tr>
                          <tr>
                            <td>  </td>
                            <td>  </td>
                            <td> Late Fees </td>
                            <td> 0 </td>
                          </tr>
                          <tr>
                            <td>  </td>
                            <td>  </td>
                            <td> Total </td>
                            <td> MVR 600 </td>
                          </tr>
                   </div>
              </tbody>
            </table>
          </form>
          </div>
        </div>
        <hr></hr>
            <section>
                Payment Summary
                   <p> {{($dataTypeContent->status)==='Unpaid'?'This student has to pay first':'This student has paid in full.'}}</p>
                <div class="notification">
                  <!-- If we are editing -->
                  @if(isset($dataTypeContent->id))
                    <p class="hidden"> The student has paid in full and the amount is MVR 600 and there are no further outstandings  </p>
                    <button type="submit" class="btn btn-primary button
                       {{($dataTypeContent->status)==='Unpaid'?'hidden':'show'}}"
                      value="Print"
                      onclick="$('#divToPrint').printThis();" > Print </button>
                  @endif

              </div>
            </section>
    <script src="{{ asset('js/printThis.js') }}"></script>

    @if($isModelTranslatable)
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
    <script src="{{ voyager_asset('lib/js/ace/ace.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_ace_editor.js') }}"></script>
    <script src="{{ voyager_asset('js/slugify.js') }}"></script>

    <!-- <script type="text/javascript">
      function PrintDiv() {
         var divToPrint = document.getElementById('divToPrint');
         var popupWin = window.open('', '_blank', 'width=300,height=300');
         popupWin.document.open();
         popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
          popupWin.document.close();
              }
   </script> -->
@stop

</div>
