@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@if(isset($dataTypeContent->id))
    @section('page_title','Edit '.$dataType->display_name_singular)
@else
    @section('page_title','Add '.$dataType->display_name_singular)
@endif

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <button class="btn btn-info" onclick="return sectionClick()">Section</button>
        <button class="btn btn-info" onclick="return batchClick()">Batch/Class</button>
        <button class="btn btn-info" onclick="return studentClick()">Student</button>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $dataType->display_name_singular }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($dataTypeContent->id))
                        {{ method_field("PUT") }}
                        @endif

                                <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                            <div class="col-md-6">
                                <div class="form-group" id="sectionIdField">
                                    <label for="section_id">Section</label>
                                    <select name="section_id" id="section_id" class="form-control select2" >
                                        <option value="">--Select an option--</option>
                                        @foreach(App\Section::all() as $section)
                                            <option value="{{ $section->id }}"> {{ $section->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group" style="display: none;" id="batchIdField">
                                    <label for="batch_id">Batch/Class</label>
                                    <select name="batch_id" id="batch_id" class="form-control select2" >
                                        <option value="">--Select an option--</option>
                                        @foreach(App\Batch::all() as $batch)
                                            <option value="{{ $batch->id }}"> {{ $batch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group" style="display: none;" id="studentIdField">
                                    <label for="student_id">Student</label>
                                    <select name="student_id" id="student_id" class="form-control select2" >
                                        <option value="">--Select an option--</option>
                                        @foreach(App\Student::all() as $student)
                                            <option value="{{ $student->id }}"> {{ $student->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fee_id">Fee Category</label>
                                        <select name="fee_id" id="fee_id" class="form-control select2" >
                                            <option value="">--Select an option--</option>
                                            @foreach(App\Fee::all() as $fee)
                                                <option value="{{ $fee->id }}"> {{ $fee->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="from_date">From Date</label>
                                        <input type="month" name="from_date" id="from_date" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="to_date">To Date</label>
                                        <input type="month" name="to_date" id="to_date" class="form-control" required>
                                    </div>
                                </div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are You Sure</h4>
                </div>

                <div class="modal-body">
                    <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
    <script>
        function sectionClick(){
            document.querySelector('#batchIdField').style.display = 'none';
            document.querySelector('#studentIdField').style.display = 'none';
        }

        function batchClick(){
            document.querySelector('#batchIdField').style.display = '';
            document.querySelector('#studentIdField').style.display = 'none';
        }

        function studentClick(){
            document.querySelector('#batchIdField').style.display = '';
            document.querySelector('#studentIdField').style.display = '';
        }
    </script>
@stop

@section('javascript')
    <script>

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });


            $('[data-toggle="tooltip"]').tooltip();

        });

        //on change section
        $('#sectionIdField').change(function(){
            var section_id = $("select[name='section_id']").val();
            $.ajax({
                type: "POST",
                url: "/api/load-batches",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: { section_id: section_id},
                success: function(data){
                    $("#batch_id").empty();
                    $("#batch_id").append($("<option />").val("").text("--Select an option--").attr('selected','selected'));
                        $.each(data, function() { //Filling each option
                        $("#batch_id").append($("<option />").val(this.id).text(this.name));
                    });
                }
            });

            $.ajax({
                type: "POST",
                url: "/api/load-fee-categories",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: { section_id: section_id},
                success: function(data){
                    $("#fee_id").empty();
                    $("#fee_id").append($("<option />").val("").text("--Select an option--").attr('selected','selected'));
                    $.each(data, function() { //Filling each option
                        $("#fee_id").append($("<option />").val(this.id).text(this.name));
                    });
                }
            });
        });

        //on change batch/class
        $('#batchIdField').change(function(){
            var batch_id = $("select[name='batch_id']").val();
            $.ajax({
                type: "POST",
                url: "/api/load-students",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: { batch_id: batch_id},
                success: function(data){
                    $("#student_id").empty();
                    $("#student_id").append($("<option />").val("").text("--Select an option--").attr('selected','selected'));
                    $.each(data, function() { //Filling each option
                        $("#student_id").append($("<option />").val(this.id).text(this.name));
                    });
                }
            });
        });

    </script>
@stop
