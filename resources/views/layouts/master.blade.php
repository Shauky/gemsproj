<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Styles -->
    <link rel="stylesheet" href=" {{ asset('css/bulma.css') }}" >
    <link rel="stylesheet" href=" {{ asset('css/semanticui.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('datatables/DataTables-1.10.15/css/dataTables.semanticui.min.css') }}">

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @if (Auth::check())
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                @endif
            </div>
        @endif
        <div class="links">

       </div>

            <div class="container">
                @yield('content')
            </div>

    </div>

       <!-- jQuery -->
       <script src="{{ asset('js/jquery-1.12.4.js') }}"></script>
       <!-- DataTables -->
       <script src="{{ asset('datatables/DataTables-1.10.15/js/jquery.dataTables.min.js') }}"></script>
       <script src="{{ asset('datatables/DataTables-1.10.15/js/dataTables.semanticui.min.js') }}"></script>
       <script src="{{ asset('js/semanticui.min.js') }}"></script>
       <script src="{{ asset('js/printThis.js') }}"></script>
            <script>
                $(document).ready(function() {
                    $('#salhitable').DataTable();
                } );
            </script>
       <!-- App scripts -->

       @stack('scripts')
</body>
</html>
