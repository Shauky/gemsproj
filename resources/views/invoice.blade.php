@extends('layouts.master')

@section('content')
<table class="table table-bordered" id="invoice-table">
      <thead>
          <tr>
              <th>Id</th>
              <th>Batch Id</th>
              <th>Student Id</th>
              <th>Payment Id</th>
          </tr>
      </thead>
      <tbody>
        @foreach($invoiceData as $data)
        <tr>
          <td>{{$data->batch_id}}</td>
          <td>{{$data->student_id}}</td>
          <td>{{$data->payment_id}}</td>
          <td>
            <button class="edit-modal btn btn-info"
              data-info="{{$data->id}},{{$data->first_name}},{{$data->batch_id}},{{$data->email}}">
              <span class="glyphicon glyphicon-edit"></span> Edit
            </button>
            <!-- <button class="delete-modal btn btn-danger"
              data-info="{{$data->id}},{{$data->first_name}},{{$data->batch_id}},{{$data->email}}">
              <span class="glyphicon glyphicon-trash"></span> Delete
          </button> -->
          <!-- <td> @if(Count($data->invoiceId) > 0)
               @foreach($data->invoiceId as $invoice)
                      <span>
                      <?php $date = date_create($invoice->due_date); echo (date_format($date, 'F Y')); ?></span> :
                      <a href="">{{ $invoice->invoice_number }}</a> <br>
               @endforeach
              @endif
          </td> -->

        </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <script src="datatables/datatables.min.js"></script>
  <script>
  $(document).ready(function() {
      $('#invoice-table').DataTable();
    } )
  </script>
@stop
@push('scripts')
