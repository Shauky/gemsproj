<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gems Project</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="datatables/datatables.min.css">

        <!-- Scripts -->

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif
            <div class="links">
                <!-- <a href="https://laravel.com/docs">Documentation</a>
                <a href="https://laracasts.com">Laracasts</a>
                <a href="https://laravel-news.com">News</a>
                <a href="https://forge.laravel.com">Forge</a>
                <a href="https://github.com/laravel/laravel">GitHub</a> -->
           </div>

            <div class="content">
                <div class="title m-b-md">
                    Little Gems
                </div>
                <table id="salhitable">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Batch</th>
                      <th>Email</th>
                      <th>Mobile</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($studentData as $data)
                    <tr>
                      <td>{{$data->first_name}}</td>
                      <td>{{$data->batch_id}}</td>
                      <td>{{$data->email}}</td>
                      <td>{{$data->mobile}}</td>
                      <td>
                        <button class="edit-modal btn btn-info"
                          data-info="{{$data->id}},{{$data->first_name}},{{$data->batch_id}},{{$data->email}}">
                          <span class="glyphicon glyphicon-edit"></span> Edit
                        </button>
                        <!-- <button class="delete-modal btn btn-danger"
                          data-info="{{$data->id}},{{$data->first_name}},{{$data->batch_id}},{{$data->email}}">
                          <span class="glyphicon glyphicon-trash"></span> Delete
                      </button> -->
                    </td>
                    </tr>
                  @endforeach
                </tbody>
                </table>
            </div>
        </div>
        <script src="datatables/datatables.min.js"></script>
        <script>
        $(document).ready(function() {
            $('#salhitable').DataTable();
          } )
        </script>

    </body>
</html>
