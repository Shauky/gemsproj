<?php $__env->startSection('content'); ?>
<table class="table table-bordered" id="invoice-table">
      <thead>
          <tr>
              <th>Id</th>
              <th>Batch Id</th>
              <th>Student Id</th>
              <th>Payment Id</th>
          </tr>
      </thead>
      <tbody>
        <?php $__currentLoopData = $invoiceData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td><?php echo e($data->batch_id); ?></td>
          <td><?php echo e($data->student_id); ?></td>
          <td><?php echo e($data->payment_id); ?></td>
          <td>
            <button class="edit-modal btn btn-info"
              data-info="<?php echo e($data->id); ?>,<?php echo e($data->first_name); ?>,<?php echo e($data->batch_id); ?>,<?php echo e($data->email); ?>">
              <span class="glyphicon glyphicon-edit"></span> Edit
            </button>
            <!-- <button class="delete-modal btn btn-danger"
              data-info="<?php echo e($data->id); ?>,<?php echo e($data->first_name); ?>,<?php echo e($data->batch_id); ?>,<?php echo e($data->email); ?>">
              <span class="glyphicon glyphicon-trash"></span> Delete
          </button> -->
          <!-- <td> <?php if(Count($data->invoiceId) > 0): ?>
               <?php $__currentLoopData = $data->invoiceId; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <span>
                      <?php $date = date_create($invoice->due_date); echo (date_format($date, 'F Y')); ?></span> :
                      <a href=""><?php echo e($invoice->invoice_number); ?></a> <br>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
          </td> -->

        </td>
        </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
  </table>
  <script src="datatables/datatables.min.js"></script>
  <script>
  $(document).ready(function() {
      $('#invoice-table').DataTable();
    } )
  </script>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>