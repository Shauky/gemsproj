<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Students</div>

                <div class="panel-body">
                    <table id="salhitable" class="ui celled table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>Name</td>
                            <td>Section</td>
                            <td>Email</td>
                            <td>Pending Invoice</td>
                        </tr>
                        </thead>
                       <tbody>
                       <?php $__currentLoopData = $studentData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <tr>
                               <td><?php echo e($student->firstName); ?></td>
                               <td><?php echo e($student->sectionId); ?></td>
                               <td><?php echo e($student->email); ?></td>
                               <td> <?php if(Count($student->invoiceId) > 0): ?>
                                    <?php $__currentLoopData = $student->invoiceId; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <span><?php $date = date_create($invoice->due_date); echo (date_format($date, 'F Y')); ?></span> :
                                           <a href=""><?php echo e($invoice->invoice_number); ?></a> <br>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </td>
                           </tr>
                       </tbody>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>