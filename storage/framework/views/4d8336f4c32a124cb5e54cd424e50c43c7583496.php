<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">

    <!-- Styles -->
    <link rel="stylesheet" href=" <?php echo e(asset('css/bulma.css')); ?>" >
    <link rel="stylesheet" href=" <?php echo e(asset('css/semanticui.min.css')); ?>" >
    <link rel="stylesheet" href="<?php echo e(asset('datatables/DataTables-1.10.15/css/dataTables.semanticui.min.css')); ?>">

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <?php if(Route::has('login')): ?>
            <div class="top-right links">
                <?php if(Auth::check()): ?>
                    <a href="<?php echo e(url('/home')); ?>">Home</a>
                <?php else: ?>
                    <a href="<?php echo e(url('/login')); ?>">Login</a>
                    <a href="<?php echo e(url('/register')); ?>">Register</a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="links">

       </div>

            <div class="container">
                <?php echo $__env->yieldContent('content'); ?>
            </div>

    </div>

       <!-- jQuery -->
       <script src="<?php echo e(asset('js/jquery-1.12.4.js')); ?>"></script>
       <!-- DataTables -->
       <script src="<?php echo e(asset('datatables/DataTables-1.10.15/js/jquery.dataTables.min.js')); ?>"></script>
       <script src="<?php echo e(asset('datatables/DataTables-1.10.15/js/dataTables.semanticui.min.js')); ?>"></script>
       <script src="<?php echo e(asset('js/semanticui.min.js')); ?>"></script>
       <script src="<?php echo e(asset('js/printThis.js')); ?>"></script>
            <script>
                $(document).ready(function() {
                    $('#salhitable').DataTable();
                } );
            </script>
       <!-- App scripts -->

       <?php echo $__env->yieldPushContent('scripts'); ?>
</body>
</html>
