-- MySQL dump 10.13  Distrib 5.7.18, for osx10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: gems
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `batches`
--

DROP TABLE IF EXISTS `batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `session_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `batches_section_id_index` (`section_id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batches`
--

LOCK TABLES `batches` WRITE;
/*!40000 ALTER TABLE `batches` DISABLE KEYS */;
INSERT INTO `batches` VALUES (1,'BNUR-A',1,'Morning 8:am to 10:am','2017-05-15 03:34:08','2017-05-15 03:34:08'),(2,'BNUR-B',1,'12:00 - 15:00','2017-07-13 08:24:51','2017-07-13 08:24:51'),(3,'Nur-H',2,'Morning 8:am to 10:am','2017-07-17 00:18:49','2017-07-17 00:18:49');
/*!40000 ALTER TABLE `batches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Post category','post-category','2017-05-07 08:38:04','2017-05-07 08:38:04'),(2,NULL,2,'featured posts','featured-posts','2017-05-07 08:39:53','2017-05-07 08:39:53');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'author_id','text','Author',1,0,1,1,0,1,NULL,2),(3,1,'category_id','text','Category',0,0,1,1,1,0,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',3),(4,1,'title','text','Title',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',4),(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,NULL,5),(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',6),(7,1,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(8,1,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',8),(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,NULL,9),(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,NULL,10),(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,NULL,12),(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,NULL,13),(14,2,'id','number','id',1,0,0,0,0,0,NULL,1),(15,2,'author_id','text','author_id',1,0,0,0,0,0,NULL,2),(16,2,'title','text','title',1,1,1,1,1,1,'{\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/sections\"}}',3),(17,2,'excerpt','text_area','excerpt',0,0,1,1,1,1,NULL,4),(18,2,'body','rich_text_box','body',0,0,1,1,1,1,NULL,5),(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),(20,2,'meta_description','text','meta_description',0,0,1,1,1,1,NULL,7),(21,2,'meta_keywords','text','meta_keywords',0,0,1,1,1,1,NULL,8),(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(23,2,'created_at','timestamp','created_at',0,1,1,0,0,0,NULL,10),(24,2,'updated_at','timestamp','updated_at',0,0,0,0,0,0,NULL,11),(25,2,'image','image','image',0,1,1,1,1,1,NULL,12),(26,3,'id','number','id',1,0,0,0,0,0,'',1),(27,3,'name','text','name',1,1,1,1,1,1,'',2),(28,3,'email','text','email',1,1,1,1,1,1,'',3),(29,3,'password','password','password',1,0,0,1,1,0,'',4),(30,3,'remember_token','text','remember_token',0,0,0,0,0,0,'',5),(31,3,'created_at','timestamp','created_at',0,1,1,0,0,0,'',6),(32,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(33,3,'avatar','image','avatar',0,1,1,1,1,1,'',8),(34,5,'id','number','id',1,0,0,0,0,0,'',1),(35,5,'name','text','name',1,1,1,1,1,1,'',2),(36,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(37,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(38,4,'id','number','id',1,0,0,0,0,0,'',1),(39,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(40,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),(41,4,'name','text','name',1,1,1,1,1,1,'',4),(42,4,'slug','text','slug',1,1,1,1,1,1,'',5),(43,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),(44,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(45,6,'id','number','id',1,0,0,0,0,0,'',1),(46,6,'name','text','Name',1,1,1,1,1,1,'',2),(47,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(48,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(49,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(50,1,'seo_title','text','seo_title',0,1,1,1,1,1,NULL,14),(51,1,'featured','checkbox','featured',1,1,1,1,1,1,NULL,15),(52,3,'role_id','text','role_id',1,1,1,1,1,1,'',9),(97,13,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(98,13,'name','text','Name',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',2),(99,13,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(100,13,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(132,20,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(133,20,'name','text','Name',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',2),(134,20,'section_id','select_dropdown','Section',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}},\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/sections\"}}',3),(135,20,'session_time','text','Session Time',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',4),(136,20,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,5),(137,20,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(138,21,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(139,21,'name','text','Name',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',2),(140,21,'address','text','Address',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',3),(141,21,'dob','date','Dob',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',4),(142,21,'parent','text','Parent',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',5),(143,21,'email','text','Email',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',6),(144,21,'number','number','Number',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',7),(145,21,'joined_date','date','Joined Date',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',8),(146,21,'blood_group','text','Blood Group',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',9),(147,21,'section_id','select_dropdown','Section',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}},\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/sections\"}}',10),(148,21,'batch_id','select_dropdown','Class/Batch',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}},\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/batches\"}}',11),(149,21,'status','select_dropdown','Status',0,1,1,1,1,1,'{\"default\":\"1\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}',13),(150,21,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,14),(151,21,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,15),(152,21,'leave_date','date','Leave Date',0,1,1,1,1,1,'{\"null\":\"\"}',12),(174,25,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(175,25,'name','text','Name',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',2),(178,25,'total_amount','text','Total Amount',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}}}',4),(179,25,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,5),(180,25,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(182,25,'section_id','select_dropdown','Section',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}},\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/sections\"}}',3),(183,26,'id','text','Id',1,0,0,0,0,0,NULL,1),(184,26,'invoice_number','text','Invoice Number',1,1,1,0,1,1,NULL,2),(185,26,'student_id','select_dropdown','Student Name',0,1,1,0,1,1,'{\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/students\"}}',3),(186,26,'section_id','select_dropdown','Section Name',1,1,1,0,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}},\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/sections\"}}',4),(187,26,'batch_id','select_dropdown','Batch/Class',0,1,1,0,1,1,'{\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/batches\"}}',5),(188,26,'fee_id','select_dropdown','Fee category',1,1,1,0,1,1,'{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"This :attribute field is a must.\"}},\"relationship\":{\"key\":\"id\",\"label\":\"name\",\"page_slug\":\"admin/fees\"}}',6),(189,26,'due_date','date','Due Date',1,1,1,0,1,1,NULL,7),(190,26,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"null\",\"options\":{\"Unpaid\":\"Unpaid\",\"Paid\":\"Paid\"}}',9),(191,26,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,10),(192,26,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,11),(193,26,'invoice_date','date','Invoice Date',1,1,1,1,1,1,NULL,8);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post',NULL,NULL,1,0,'2017-05-07 03:56:26','2017-07-20 04:47:05'),(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,NULL,1,0,'2017-05-07 03:56:26','2017-07-20 04:45:18'),(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','','',1,0,'2017-05-07 03:56:26','2017-05-07 03:56:26'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category','','',1,0,'2017-05-07 03:56:26','2017-05-07 03:56:26'),(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu','','',1,0,'2017-05-07 03:56:26','2017-05-07 03:56:26'),(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role','','',1,0,'2017-05-07 03:56:26','2017-05-07 03:56:26'),(13,'sections','sections','Section','Sections',NULL,'App\\Section',NULL,'All sections',1,0,'2017-05-13 06:09:17','2017-05-13 06:09:17'),(20,'batches','batches','Batch','Batches',NULL,'App\\Batch',NULL,NULL,1,0,'2017-05-15 03:12:17','2017-05-15 03:12:17'),(21,'students','students','Student','Students',NULL,'App\\Student',NULL,NULL,1,0,'2017-05-15 06:18:02','2017-05-15 06:18:02'),(25,'fees','fees','Fee','Fees','voyager-double-right','App\\Fee',NULL,NULL,1,0,'2017-07-12 09:27:45','2017-07-12 09:27:45'),(26,'invoices','invoices','Invoice','Invoices',NULL,'App\\Invoice','InvoiceController',NULL,1,0,'2017-07-30 00:41:07','2017-07-30 00:47:57');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `total_amount` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fees_batch_id_index` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
INSERT INTO `fees` VALUES (4,'Baby Nursery School Fee',1,360,'2017-07-15 03:21:56','2017-07-15 03:21:56'),(5,'Nursery School Fee',2,450,'2017-07-17 00:23:05','2017-07-17 00:23:05'),(6,'Kindergarten tution fee',3,600,'2017-07-26 03:36:48','2017-07-26 03:36:48');
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` int(10) unsigned DEFAULT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `batch_id` int(10) unsigned DEFAULT NULL,
  `fee_id` int(10) unsigned NOT NULL,
  `due_date` datetime NOT NULL,
  `invoice_date` datetime NOT NULL,
  `status` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_student_id_index` (`student_id`),
  KEY `invoice_section_id_index` (`section_id`),
  KEY `invoice_classe_i_index` (`batch_id`),
  KEY `invoice_fee_id_index` (`fee_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (1,'INV1',1,1,1,4,'2017-08-17 00:00:00','2017-08-03 00:00:00','Paid','2017-08-03 04:29:18','2017-11-05 06:56:36'),(2,'INV2',3,1,1,4,'2017-08-29 00:00:00','2017-08-15 00:00:00','Paid','2017-08-15 00:34:12','2017-11-08 10:39:23');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','/admin','_self','voyager-dashboard','#000000',NULL,1,'2017-05-07 03:56:26','2017-05-07 13:03:34',NULL,''),(4,1,'Users','/admin/users','_self','voyager-person',NULL,NULL,8,'2017-05-07 03:56:26','2017-06-29 01:03:58',NULL,NULL),(9,1,'Menu Builder','/admin/menus','_self','voyager-list',NULL,8,1,'2017-05-07 03:56:26','2017-05-07 07:46:20',NULL,NULL),(10,1,'Database','/admin/database','_self','voyager-data',NULL,8,2,'2017-05-07 03:56:26','2017-05-07 07:46:20',NULL,NULL),(11,1,'Settings','/admin/settings','_self','voyager-settings',NULL,NULL,9,'2017-05-07 03:56:26','2017-06-29 01:03:58',NULL,NULL),(12,1,'Students','admin/students','_self','voyager-study','#000000',NULL,4,'2017-05-07 07:46:03','2017-05-15 05:10:40',NULL,''),(13,1,'Sections','admin/sections','_self','voyager-double-right','#000000',NULL,2,'2017-05-07 09:21:49','2017-05-13 07:04:27',NULL,''),(15,1,'Classes','/admin/batches','_self','voyager-double-right','#000000',NULL,3,'2017-05-07 12:00:44','2017-05-15 03:30:19',NULL,''),(16,1,'Fees','/admin/fees','_self','voyager-double-right','#000000',NULL,6,'2017-05-13 07:06:17','2017-06-29 01:04:15',NULL,''),(17,1,'invoices','/admin/invoices','_self','voyager-window-list','#000000',NULL,7,'2017-06-29 01:01:14','2017-06-29 01:04:15',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2017-05-07 03:56:26','2017-05-07 03:56:26');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_01_01_000000_create_pages_table',1),(6,'2016_01_01_000000_create_posts_table',1),(7,'2016_02_15_204651_create_categories_table',1),(8,'2016_05_19_173453_create_menu_table',1),(9,'2016_10_21_190000_create_roles_table',1),(10,'2016_10_21_190000_create_settings_table',1),(11,'2016_11_30_135954_create_permission_table',1),(12,'2016_11_30_141208_create_permission_role_table',1),(13,'2016_12_26_201236_data_types__add__server_side',1),(14,'2017_01_13_000000_add_route_to_menu_items_table',1),(15,'2017_01_14_005015_create_translations_table',1),(16,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),(17,'2017_01_15_000000_create_permission_groups_table',1),(18,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(19,'2017_03_06_000000_add_controller_to_data_types_table',1),(20,'2017_04_21_000000_add_order_to_data_rows_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(65,1),(66,1),(67,1),(68,1),(69,1),(90,1),(91,1),(92,1),(93,1),(94,1),(95,1),(96,1),(97,1),(98,1),(99,1),(115,1),(116,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(123,1),(124,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(2,'browse_database',NULL,'2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(3,'browse_media',NULL,'2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(4,'browse_settings',NULL,'2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(5,'browse_menus','menus','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(6,'read_menus','menus','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(7,'edit_menus','menus','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(8,'add_menus','menus','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(9,'delete_menus','menus','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(10,'browse_pages','pages','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(11,'read_pages','pages','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(12,'edit_pages','pages','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(13,'add_pages','pages','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(14,'delete_pages','pages','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(15,'browse_roles','roles','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(16,'read_roles','roles','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(17,'edit_roles','roles','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(18,'add_roles','roles','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(19,'delete_roles','roles','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(20,'browse_users','users','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(21,'read_users','users','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(22,'edit_users','users','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(23,'add_users','users','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(24,'delete_users','users','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(25,'browse_posts','posts','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(26,'read_posts','posts','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(27,'edit_posts','posts','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(28,'add_posts','posts','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(29,'delete_posts','posts','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(30,'browse_categories','categories','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(31,'read_categories','categories','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(32,'edit_categories','categories','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(33,'add_categories','categories','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(34,'delete_categories','categories','2017-05-07 03:56:26','2017-05-07 03:56:26',NULL),(65,'browse_sections','sections','2017-05-13 06:09:17','2017-05-13 06:09:17',NULL),(66,'read_sections','sections','2017-05-13 06:09:17','2017-05-13 06:09:17',NULL),(67,'edit_sections','sections','2017-05-13 06:09:17','2017-05-13 06:09:17',NULL),(68,'add_sections','sections','2017-05-13 06:09:17','2017-05-13 06:09:17',NULL),(69,'delete_sections','sections','2017-05-13 06:09:17','2017-05-13 06:09:17',NULL),(90,'browse_batches','batches','2017-05-15 03:12:17','2017-05-15 03:12:17',NULL),(91,'read_batches','batches','2017-05-15 03:12:17','2017-05-15 03:12:17',NULL),(92,'edit_batches','batches','2017-05-15 03:12:17','2017-05-15 03:12:17',NULL),(93,'add_batches','batches','2017-05-15 03:12:17','2017-05-15 03:12:17',NULL),(94,'delete_batches','batches','2017-05-15 03:12:17','2017-05-15 03:12:17',NULL),(95,'browse_students','students','2017-05-15 06:18:02','2017-05-15 06:18:02',NULL),(96,'read_students','students','2017-05-15 06:18:02','2017-05-15 06:18:02',NULL),(97,'edit_students','students','2017-05-15 06:18:02','2017-05-15 06:18:02',NULL),(98,'add_students','students','2017-05-15 06:18:02','2017-05-15 06:18:02',NULL),(99,'delete_students','students','2017-05-15 06:18:02','2017-05-15 06:18:02',NULL),(115,'browse_fees','fees','2017-07-12 09:27:46','2017-07-12 09:27:46',NULL),(116,'read_fees','fees','2017-07-12 09:27:46','2017-07-12 09:27:46',NULL),(117,'edit_fees','fees','2017-07-12 09:27:46','2017-07-12 09:27:46',NULL),(118,'add_fees','fees','2017-07-12 09:27:46','2017-07-12 09:27:46',NULL),(119,'delete_fees','fees','2017-07-12 09:27:46','2017-07-12 09:27:46',NULL),(120,'browse_invoices','invoices','2017-07-30 00:41:08','2017-07-30 00:41:08',NULL),(121,'read_invoices','invoices','2017-07-30 00:41:08','2017-07-30 00:41:08',NULL),(122,'edit_invoices','invoices','2017-07-30 00:41:08','2017-07-30 00:41:08',NULL),(123,'add_invoices','invoices','2017-07-30 00:41:08','2017-07-30 00:41:08',NULL),(124,'delete_invoices','invoices','2017-07-30 00:41:08','2017-07-30 00:41:08',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2017-05-07 03:56:26','2017-05-07 03:56:26'),(2,'user','Normal User','2017-05-07 03:56:26','2017-05-07 03:56:26');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'Baby Nursery','2017-05-13 07:50:23','2017-05-13 07:50:23'),(2,'Nursery','2017-05-13 08:05:05','2017-05-13 08:05:05'),(3,'Kindergarten','2017-05-13 08:05:22','2017-05-13 08:05:22');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'admin_title','Dashboard Title','Gems Preschool',NULL,'text',0),(2,'admin_description','Dashboard Description','Gems Preschool Student fee management application',NULL,'text',1),(3,'admin_bg_image','Login background image','settings/September2017/48zEp7EUPxNxiaKe4IOn.png',NULL,'image',2),(4,'admin_logo_img','Change logo','',NULL,'image',3),(5,'admin_loader','loading icon','settings/May2017/EuV3YOVmVdCU32JSDhOI.gif',NULL,'image',4),(7,'admin_icon_image','admin icon image','settings/May2017/iuNqNlHoiUoVOYX9uFuG.png',NULL,'file',5);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `blood_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `batch_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `nic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `students_section_id_index` (`section_id`),
  KEY `students_batch_id_index` (`batch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Hike','Victoria US, Middle Way Road','2017-05-16','Shuaib','suaib@email.com',7777778,'2017-05-25','O+',1,1,1,'2017-05-15 06:36:21','2017-05-15 06:42:40',NULL,NULL),(2,'Nike','Flat - 03-03-03','2014-07-16','Shausan','shausan@gamil.com',9665514,'2016-01-10','A+',2,3,1,'2017-07-17 00:22:13','2017-07-17 00:22:13',NULL,NULL),(3,'Like','Flat - 03-03-04','2015-01-05','Like mother','likemom@gmail.com',75842141,'2017-11-09','O+',1,1,1,'2017-07-20 01:35:54','2017-07-20 01:35:54',NULL,NULL);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'gemschool','gems@email.com','users/May2017/7fXJA8u8CbcDPxmn8NMe.jpg','cc03e747a6afbbcbf8be7668acfebee5','QyvJN9rdYj5wMT6JVVHhOHsEZlBphQZmRDhjvh71wKZYelBXKoxrUgoaret0','2017-05-07 04:04:04','2017-05-07 13:06:12'),(2,1,'fazal','fazal@email.com','users/default.png','$2y$10$Vr1w8Yyq0uXcwYWHaeYKceHUfMHirWxo7fpaQP/PawLi3DKteAGVq','GKzdx1Db0M5KkEF5c9lWs4FvWaaIx6i1dqRAog5PKr9ZksTjZjACkF1cZbAY','2017-05-13 01:51:07','2017-05-13 01:51:07'),(3,1,'shauky@email.com','your@email.com','users/default.png','$2y$10$iOVq6dIJSTpcVedX6G59JOjTl08xOoVoFK5KaAm8mjlRYAUhAsJWC',NULL,'2017-10-03 23:08:18','2017-10-03 23:08:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-27 13:16:04
