Little Gems Students Fee Management System

Contents
Introduction about the system	2
Main features of the system	3
User documentation – Back office Management	4
Dashboard	4
Sections	4
Batch/Class	4
Students	4
Fees	4
Invoices	4
Users	4
Settings	4


Introduction about the system
This system is made focusing on paying students fees on monthly and quarterly based. The main aim of the system is to manage student invoices and maintain student fees regularly on every months. This system is divided in to two areas. One that manage critical changes settings and overall enrollment startup of the system are done by using the dashboard. This area can accessible permissions given users only. The other area is designed for the front end part of the system, whereas registered users are involving such as paying student fees and print receipts.


Main features of the system
What are the features available in the system?

Back office management features are available in the dashboard.

Dashboard
Display Main operation of the system
Manage user roles and control permissions for users

Sections
Manage students belonging various sections

Classes
Manage Students Classes

Students
Manage Students

Fees
Manage Student sections fees

Invoices
Manage Student Invoices

Users
Manage users of the systems

Settings
Manage overall settings of the systems

Day to operation carried out by front end system. Normal registered users who have an accessible permission can access the system day to day operation and pay student fees and print receipt for the particular invoice.  Usually this area, normal user can have access, but registered users only.

User documentation – Back office Management

Dashboard


Sections


Batch/Class


Students


Fees


Invoices


Users


Settings
