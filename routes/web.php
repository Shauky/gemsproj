<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $studentData = App\Student::get();
    return view('home',['studentData'  => $studentData]);
    // dd($studentData);

});

Route::get('/invoice', function() {
  $invoiceData = App\Invoice::get();
  return view('invoice', ['invoiceData' => $invoiceData]);

});

// Route::get('/graph', function() {
//    return view('hello');
// });


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
