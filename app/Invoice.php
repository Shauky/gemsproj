<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Invoice extends Model
{
    protected $table = "invoices";

    protected $fillable = ['invoice_number', 'student_id', 'section_id', 'batch_id', 'fee_id', 'due_date', 'invoice_date','status'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function sectionId()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function batchId()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
    }

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
//     */
//    public function studentId()
//    {
//        return $this->belongsTo('App\Student', 'student_id');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function feeId()
    {
        return $this->belongsTo('App\Fee', 'fee_id');
    }

    public function studentId()
    {
        return $this->belongsTo('App\Student', 'student_id');
    }


    public function scopeUnpaidInvoice($query)
    {
        return $query->where('status', 'unpaid');
    }

    public function scopePaidInvoice($query)
    {
        return $query->where('status', 'paid');
    }

    public function scopeFilterByInvoice($query, Request $request,$date, $studentId=null)
    {
        $from_year = date('Y', strtotime($request->get('from_date')));

        $from_month = date('m', strtotime($request->get('from_date')));

        $to_year = date('Y', strtotime($request->get('to_date')));

        $to_month = date('m', strtotime($request->get('to_date')));

        $year = date('Y', strtotime($date));
        $month = date('m', strtotime($date));

//        dd($studentId,$year,$month,$request->get('fee_id'),$request->get('student_id'));
        return $query->whereYear('invoice_date', '=',  $year)->whereMonth('invoice_date', '=',  $month)
            ->where('student_id', '=', $request->get('student_id'))->orWhere('student_id', '=', $studentId)
            ->where('fee_id', '=', $request->get('fee_id'));
    }

}
