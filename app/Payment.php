<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Payment extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function batchId()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
//        return $this->belongsTo(Voyager::modelClass('Batch'), 'id', 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function studentId()
    {
        return $this->belongsTo('App\Student', 'student_id');
//        return $this->belongsTo(Voyager::modelClass('Batch'), 'id', 'batch_id');
    }
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function userId()
    {
        return $this->belongsTo('App\User', 'user_id');
//        return $this->belongsTo(Voyager::modelClass('Batch'), 'id', 'batch_id');
    }
}
