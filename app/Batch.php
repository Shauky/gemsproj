<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


class Batch extends Model
{
//    use Translatable;
//
//    protected $translatable = ['name', 'session_time', 'section_id'];

    protected $table = 'batches';

//    protected $fillable = ['name', 'session_time', 'section_id'];

    public function students()
    {
        return $this->hasMany('App\Student', 'batch_id');
//        return $this->hasMany(Voyager::modelClass('Student'))
//            ->orderBy('created_at', 'DESC');
    }

    public function sectionId()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }

//    //pivot table connection
//    public function feeId(){
//        return $this->belongsToMany(Fee::class);
//    }


}
