<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FeeCategory extends Model
{
    protected $table = 'fee_categories';
    public function fees()
    {
        return $this->hasMany('App\Fee', 'fee_category_id');
    }
}
