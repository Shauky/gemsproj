<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Fee extends Model
{
    protected $table = 'fees';

    public function sectionId(){
        return $this->belongsTo('App\Section', 'section_id');
    }

//    //pivot table connection
//    public function batchId(){
//    return $this->belongsToMany(Batch::class, 'batch_fee');
//    }
}