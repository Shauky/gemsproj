<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;

use TCG\Voyager\Traits\Translatable;

class Student extends Model
{
    use Translatable;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sectionId()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }

    public function batchId()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function invoiceId()
    {
        return $this->hasMany('App\Invoice', 'student_id');
    }
}
