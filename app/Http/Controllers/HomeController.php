<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Student;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::with(['sectionId', 'batchId','InvoiceId' => function ($query) {
            $query->where('status', 'unpaid');
        }])->where('status', true)->get();

        return view('home')->with(['students' =>  $students]);
    }
}
