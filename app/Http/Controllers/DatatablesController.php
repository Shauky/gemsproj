<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Student;
use Yajra\Datatables\Datatables;


class DatatablesController extends Controller
{
    /*
    * @return \Illumiate\View\view
    *
    */

    public function getIndex()
    {
      return view('datatables.index');
    }


    /*
    * @return \Illuminate\Http\JsonResponse
    *
    */

    public function invoiceData()
    {
      return Datatables::of(Invoice::query()) -> make(true);
    }
}
