<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Fee;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApiController extends Controller
{

    public function getBatches(Request $request)
    {
        return response()->json(Batch::where('section_id', $request->section_id)->get());
//        return $query = Batch::where('section_id', $request->section_id)->get();
//        $list = null;
//
//        foreach($query as $row)
//        {
//            $id = $row->id;
//            $name = $row->name;
//            $list.= "<option value='$id'>$name</option>";
//        }
//        return "<option value=''>--Select an option--</option>".$list;
    }

    public function getStudents(Request $request)
    {
        return response()->json(Student::where('batch_id', $request->batch_id)->get());
    }

    public function getFeeCategories(Request $request)
    {
        return response()->json($query = Fee::where('section_id', $request->section_id)->get());
    }


}
