<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Invoice;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Validator;

class InvoiceController extends Controller
{
    use BreadRelationshipParser;


    public function index(Request $request)
    {
        $invoiceData = Invoice::get();
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('browse_'.$dataType->name);

        $getter = $dataType->server_side ? 'paginate' : 'get';

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $relationships = $this->getRelationships($dataType);
            if ($model->timestamps) {
                $dataTypeContent = call_user_func([$model->with($relationships)->latest(), $getter]);
            } else {
                $dataTypeContent = call_user_func([$model->with($relationships)->orderBy('id', 'DESC'), $getter]);
            }

            //Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        $view = 'admin.invoice.index';

        return view($view, compact('dataType', 'dataTypeContent','isModelTranslatable','invoiceData'));
    }

    public function create(Request $request) {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
                        ? new $dataType->model_name()
                        : false;

        // Check if BREAD is Translatable
//        $isModelTranslatable = is_bread_translatable($dataTypeContent);

//dd($dataType,$dataTypeContent);
        $view = 'admin.invoice.create';
        return view($view, compact('dataType','dataTypeContent'));
    }

    public function store(Request $request)
    {

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('add_'.$dataType->name);

//        dd($request->all());

        $val = $this->validateBread($request->all(), $dataType->addRows);
//        dd($val);
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        else
        {
            // creating student invoice
            if($request->has(['section_id','batch_id','student_id']))
            {
                return $this->studentInvoiceCreate($request, $dataType);

            }
            // creating batch invoice
            elseif($request->has(['section_id','batch_id']))
            {
                return $this->batchInvoiceCreate($request, $dataType);

            }
            // creating section invoice
            else
            {
                return $this->sectionInvoiceCreate($request, $dataType);

            }
        }

    }

    //edit student invoice
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name


        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'admin.invoice.edit';

        return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function show(Request $request, $id)
    {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
      //dd($dataType);

      // Check permission
      Voyager::canOrFail('edit_'.$dataType->name);

      $relationships = $this->getRelationships($dataType);

      $dataTypeContent = (strlen($dataType->model_name) != 0)
          ? app($dataType->model_name)->with($relationships)->findOrFail($id)
          : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

          $dataTypeContent->with('studentId')->get();

      // Check if BREAD is Translatable
      $isModelTranslatable = is_bread_translatable($dataTypeContent);

      $view = 'admin.invoice.show';

      return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));

    }

    //update student invoice
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $student_invoice = Invoice::where('id', $id)->first();
        $request->merge([
            'section_id' => $student_invoice->section_id,
            'fee_id' => $student_invoice->fee_id
        ]);
//        dd($request->all(),$student_invoice);
        //Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);
//        dd($val);
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            $data->update($request->all());
//            dd($data);
            return redirect()
                ->route("voyager.{$dataType->slug}.edit", ['id' => $id])
                ->with([
                    'message'    => "Successfully Updated {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    //delete student invoice
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('delete_'.$dataType->name);

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        $data = $data->destroy($id)
            ? [
                'message'    => "Successfully Deleted Student {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => "Sorry it appears there was a problem deleting this {$dataType->display_name_singular}",
                'alert-type' => 'error',
            ];

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    /**
     * @return array
     * invoice number generate
     */
    public function invoiceNumber()
    {
    //check existing last invoice number
        if(Invoice::exists())
        {
            $last_invoice = Invoice::orderBy('invoice_number', 'desc')->first();
            $string = substr($last_invoice->invoice_number, 0, 3);
            $number = (int)substr($last_invoice->invoice_number, 3);
            return array($string, $number);
        }
        else
        {
            $string = "INV";
            $number = 0;
            return array($string, $number);
        }

    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function invoiceCreate(Request $request)
    {
        return Invoice::create($request->all());
    }

    /**
     * @param Request $request
     * @param $dataType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function batchInvoiceCreate(Request $request, $dataType)
    {
        // collect all students with batch id
        $students = Student::where([
            ['section_id', '=', $request->get('section_id')],
            ['batch_id', '=', $request->get('batch_id')],
            ['status', '=', 1]
        ])->get();

//        $existing = [];
        if(count($students) > 0) {
            $ranges =  $this->dateRange($request);

            foreach ($students as $index => $student) {
                foreach($ranges as $i => $date){
                    list($string, $number) = $this->invoiceNumber();
                    $existing = $this->checkExistInvoice($request, $student);
//                dd($existing[$index], count($existing));
                    if (!$existing) {

                        //creating due date
                        $due_date = new \DateTime($date);
                        $due_date->modify('+14 day');

                        //create invoice for selected section with selected batch no with selected student
                        $request->merge([
                            'student_id' => $student->id,
                            'invoice_number' => $string . ($number + 1),
                            'fee_id' => $request->input('fee_id'),
                            'invoice_date' => $date,
                            'due_date' => $due_date,
                            'status' => 'unpaid'
                        ]);

                        // create invoice for each student
//                        dd($request->all());
                        $this->invoiceCreate($request);
                    }
                }


            }
            return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message' => "Successfully Added New Batch Invoice",
                        'alert-type' => 'success',
                    ]);

        } else {
            return redirect()
                ->route("voyager.{$dataType->slug}.create")
                ->with([
                    'message' => "Their is no student enrolled in for the selected Batch",
                    'alert-type' => 'error',
                ]);
        }

    }

    /**
     * @param Request $request
     * @param $dataType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentInvoiceCreate(Request $request, $dataType)
    {
        // gets selected range
        $ranges =  $this->dateRange($request);
        $counter = 0;
        foreach($ranges as $index => $date)
        {
            list($string, $number) = $this->invoiceNumber();
            $existing_student_invoice = Invoice::filterByInvoice($request,$date)->exists();

            if (!$existing_student_invoice) {

                //creating due date
                $due_date = new \DateTime($date);
                $due_date->modify('+14 day');

                //create invoice for selected section with selected batch no with selected student
                $request->merge([
                    'invoice_number' => $string . ($number + 1),
                    'fee_id' => $request->input('fee_id'),
                    'invoice_date' => $date,
                    'due_date' => $due_date,
                    'status' => 'unpaid'
                ]);

                $counter++;
                $this->invoiceCreate($request);

//                return redirect()
//                    ->route("voyager.{$dataType->slug}.create")
//                    ->withInput()
//                    ->with([
//                        'message' => "Student {$dataType->display_name_singular} Exists for the selected dates",
//                        'alert-type' => 'error',
//                    ]);

            }

        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->withInput()
            ->with([
                'message' => "Student {$dataType->display_name_singular} Successfully added",
                'alert-type' => 'success',
            ]);


    }

    /**
     * @param Request $request
     * @param $dataType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sectionInvoiceCreate(Request $request, $dataType)
    {
        // collect all batches with section id
        $batches = Batch::where('section_id', '=', $request->get('section_id'))->get();

        // collect all students with section id and bacth id
        if (count($batches) > 0) {
            foreach ($batches as $b => $batch) {
                $students = Student::where([
                    ['section_id', '=', $request->get('section_id')],
                    ['batch_id', '=', $batch->id],
                    ['status', '=', 1]
                ])->get();

                if (count($students) > 0) {
                    $existing = [];
                    foreach ($students as $s => $student) {
                        $existing_section_invoice = Invoice::filterByInvoice($request, $student->id)->exists();

                        if ($existing_section_invoice) {
                            $existing = 1;
                        }

                        if (!$existing_section_invoice) {
                            list($string, $number) = $this->invoiceNumber();

                            //create invoice for selected section with selected batch no with selected student
                            $request->merge([
                                'student_id' => $student->id,
                                'invoice_number' => $string . ($number + 1),
                                'fee_id' => $request->input('fee_id'),
                                'status' => 'unpaid'
                            ]);


                            // create invoice for each student
                            $this->invoiceCreate($request);
                        }
                    }
                }

            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message' => "Successfully Added New Section {$dataType->display_name_plural} for all the batches",
                    'alert-type' => 'success',
                ]);

        } else {
            return redirect()
                ->route("voyager.{$dataType->slug}.create")
                ->with([
                    'message' => "Their is no batches in for the selected Section",
                    'alert-type' => 'error',
                ]);
        }
    }

    /**
     * @param Request $request
     * @param $student
     */
    public function checkExistInvoice(Request $request, $student)
    {
        return Invoice::filterByInvoice($request, $student->id)->exists();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function dateRange(Request $request)
    {
        $dates = [];

//        $startDateInt = new \DateInterval( "P1Y" );
        $endDateInt = new \DateInterval( "P1M" );
//
//        $from_date->sub( $startDateInt );
//        $to_date->add( $endDateInt );
//
//        $periodInt = new \DateInterval( "P1M" );
//        $period = new \DatePeriod( $from_date, $periodInt, $to_date );

        $from_date = new \DateTime($request->get('from_month'));
        $to_date = new \DateTime($request->get('to_month'));

        $interval = \DateInterval::createFromDateString('1 month');
        $to_date->add( $endDateInt );

        $period = new \DatePeriod($from_date, $interval, $to_date);

        foreach ($period as $dt ){
            $dates[] = $dt->format( "Y-m-d" );
        }
//        dd($begin,$end,$period);
//
        return $dates;


    }

}
