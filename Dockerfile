FROM php:7.0.4-fpm

RUN apt-get update -y && apt-get install -y libmcrypt-dev \
  mysql-client openssl zip unzip git --no-install-recommends \
  && docker-php-ext-install mcrypt pdo_mysql mbstring
